#pragma once
#include "Product.h"

class PerishableProduct :public Product
{
public:
	PerishableProduct(int32_t id, const std::string& name, float rawPrice, const std::string& expirationDate);
	~PerishableProduct() override = default;
	const std::string& getExpirationDate() const;
	float getPrice() const override final;
	int32_t getVAT() const override final;

private:
	std::string m_expirationDate;
};