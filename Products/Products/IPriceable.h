#pragma once
#include <cstdint>

class IPriceable
{
public:
	virtual ~IPriceable() = default;
	virtual int32_t getVAT() const = 0;
	virtual float getPrice() const = 0;
};