#include "NonperishableProduct.h"

NonperishableProduct::NonperishableProduct(int32_t id, const std::string& name, float rawPrice, Type type): m_type(type), Product(id, name, rawPrice)
{

}

#pragma region Getters
NonperishableProduct::Type NonperishableProduct::getType() const
{
	return m_type;
}
float NonperishableProduct::getPrice() const
{
	return m_rawPrice + static_cast<float>(getVAT()) * m_rawPrice / 100; //sau 100.0f fara cast
}
int32_t NonperishableProduct::getVAT() const
{
	return 19;
}
#pragma endregion Getters
