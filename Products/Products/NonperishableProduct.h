#pragma once
#include "Product.h"



class NonperishableProduct :public Product
{
public: 
	enum class Type
{
	Clothing,
	SmallAppliences,
	PersonalHygiene
};
public:
	NonperishableProduct(int32_t id, const std::string& name, float rawPrice, Type type);
	~NonperishableProduct() override = default;

	Type getType() const;
	float getPrice() const override final;              //fol final nu putem sa mai suprascriem fct mai tarziu
	int32_t getVAT() const override final;

private:
	Type m_type;
};