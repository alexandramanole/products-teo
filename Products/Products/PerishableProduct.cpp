#include "PerishableProduct.h"

PerishableProduct::PerishableProduct(int32_t id, const std::string& name, float rawPrice, const std::string& expirationDate): m_expirationDate(expirationDate) , Product(id, name, rawPrice)
{

}

#pragma region Getters
const std::string& PerishableProduct::getExpirationDate() const
{
	return m_expirationDate;
}
float PerishableProduct::getPrice() const
{
	return m_rawPrice + static_cast<float>(getVAT())* m_rawPrice / 100;
}
int32_t PerishableProduct::getVAT() const
{
	return 9;
}
#pragma endregion Getters