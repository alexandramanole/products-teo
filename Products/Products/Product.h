#pragma once
#include <string>
#include <cstdint>
#include "IPriceable.h"

class Product :public IPriceable
{
public:
	Product(int32_t id, const std::string& name, float rawPrice);

	uint16_t getID() const;           //punem const pt ca sunt metode constante in care nu se modifica membri elem curent
	const std::string& getName() const;       //pt ca are ca fiecare prim param this
	float getRawPrice() const; 

protected:
	int32_t m_id;
	std::string m_name;
	float m_rawPrice;
};